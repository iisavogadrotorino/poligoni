package poligoni;

public class Triangolo extends Poligono {
    private Triangolo() {
    }

    public Triangolo(float base, float altezza) {
        setBase(base);
        setAltezza(altezza);
        setNumLati(3);
    }
    
    public int getSpessoreBordo() {
        return 1;
    }

    float getArea() {
        return getBase() * getAltezza() / 2;
    }

    float getPerimetro() {
        return getBase() * getAltezza() * 2;
    }
    
    @Override
    public void setBase(float base) {
        if (base > 1000) {
            throw new RuntimeException("Base rettangolo troppo grande");
        }
        super.setBase(base);
     
    }
    
    @Override
    public void setAltezza(float altezza) {
        // TODO Auto-generated method stub
        super.setAltezza(altezza);
    }
}
