package poligoni;

public interface FiguraGrafica {
    Colore getColoreSfondo();
    void setColoreSfondo(Colore c);
    Colore getColoreBordo();
    int getSpessoreBordo();
}




