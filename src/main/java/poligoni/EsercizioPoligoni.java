package poligoni;

import java.util.Scanner;

public class EsercizioPoligoni {
    public static void main(String[] args) {
        System.out.println("Programma EsercizioPoligoni");

        Rettangolo r1 = new Rettangolo(5, 10);

        for (Colore c : Colore.values()) {
            System.out.println(c.toString());
        }

        leggiColoreSfondo(r1);

        // Triangolo t1 = new Triangolo(2, 10);

        Poligono p1 = r1;

        System.out.println("lati=" + p1.getNumLati() + " area=" + p1.getArea() + " tipo=" + p1.getClass().getName());

        //p1 = t1;

        System.out.println("lati=" + p1.getNumLati() + " area=" + p1.getArea() + " tipo=" + p1.getClass().getName());


        infoFigura(p1);
    }

    static void infoFigura(FiguraGrafica f) {
        System.out.print("Informazioni sulla figura grafica:");
        System.out.print("bordo spesso " + f.getSpessoreBordo() + " di colore " + f.getColoreBordo());
        System.out.print("sfondo di colore " + f.getColoreSfondo());
    }


    static void leggiColoreSfondo(FiguraGrafica f) {
        while (true) {
            try {
                System.out.print("Scegli un colore di sfondo (ROSSO,VERDE,GIALLO):");
                Scanner reader = new Scanner(System.in);
                String colore = reader.nextLine().toUpperCase();
                Colore c = Colore.valueOf(colore);
                f.setColoreSfondo(c);
                reader.close();
                return;
            } catch (Exception ex) {
                System.out.println("Hai scelto un colore non valido");
            }
        }
    }

    static void infoGiocatore(Giocatore p) {
        System.out.print("Informazioni sul giocatore:");
        System.out.print("nome " + p.getNome());
        System.out.print("cognome " + p.getCognome());
        System.out.print("punteggio " + p.getPunteggio());
    }
}
