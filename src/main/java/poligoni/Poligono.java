package poligoni;

abstract public class Poligono implements FiguraGrafica {
    private int    numLati;
    private Colore coloreBordo;
    private Colore coloreSfondo;
    private float  base;
    private float  altezza;


    public int getNumLati() {
        return numLati;
    }

    protected void setNumLati(int numLati) {
        this.numLati = numLati;
    }

    public Colore getColoreBordo() {
        return coloreBordo;
    }

    public Colore getColoreSfondo() {
        return coloreSfondo;
    }
    
    @Override
    public void setColoreSfondo(Colore c) {
        coloreSfondo = c;
    }

    public void setColoreBordo(Colore coloreBordo) {
        this.coloreBordo = coloreBordo;
    }

    public float getAltezza() {
        return altezza;
    }

    public void setAltezza(float altezza) {
        if (altezza < 0) {
            throw new RuntimeException("La altezza non può essere negativa");
        }
        this.altezza = altezza;
    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        if (base < 0) {
            throw new RuntimeException("La base non può essere negativa");
        }
        this.base = base;
    }

    abstract float getArea();

    abstract float getPerimetro();
}
