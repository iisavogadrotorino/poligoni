package poligoni;

public class Rettangolo extends Poligono {
    int spessoreBordo;
    
    private Rettangolo() {
    }

    public Rettangolo(float base, float altezza) {
        setBase(base);
        setAltezza(altezza);
        setNumLati(4);
    }

    @Override
    public int getSpessoreBordo() {
        // TODO Auto-generated method stub
        return spessoreBordo;
    }

    float getArea() {
        return getBase() * getAltezza();
    }

    float getPerimetro() {
        return getBase() * getAltezza() * 2;
    }

    @Override
    public void setBase(float base) {
        if (base > 1000) {
            throw new RuntimeException("Base rettangolo troppo grande");
        }
        super.setBase(base);

    }

    @Override
    public void setAltezza(float altezza) {
        // TODO Auto-generated method stub
        super.setAltezza(altezza);
    }
}
