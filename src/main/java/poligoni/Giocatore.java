package poligoni;


public interface Giocatore {
    Colore getNome();
    void setNome(String nome);
    String getCognome();
    int getPunteggio();
}
